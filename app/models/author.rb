class Author < ActiveRecord::Base

  before_create :create_session_token

  has_secure_password

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true, uniqueness: true


  def full_name
    "#{first_name} #{last_name}"
  end

  def self.new_session_token
    SecureRandom.urlsafe_base64
  end

  def self.digest token
    Digest::SHA1.hexdigest(token.to_s)
  end

  
  private
    def create_session_token
      self.session_token = Author.digest(Author.new_session_token)
    end
  
end
