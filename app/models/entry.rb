class Entry < ActiveRecord::Base
  extend FriendlyId

  scope :contains, -> (title) { where('entries.published = ? and LOWER(entries.title) LIKE ?', true, "%#{title.downcase}%") }

  before_validation {
    self.tag = tag.downcase
  }

  friendly_id :title, use: [:slugged, :finders]

  has_and_belongs_to_many :categories, dependent: :destroy

  belongs_to :author

  validates :title, presence: true
  validates :tag, presence: true
  validates :content, presence: true
  validates :clicks, numericality: { greater_than_or_equal_to: 0 }

end
