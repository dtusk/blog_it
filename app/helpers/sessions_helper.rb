module SessionsHelper

  def sign_in author
    session_token = Author.new_session_token
    cookies.permanent[:session_token] = session_token
    author.update_attribute(:session_token, Author.digest(session_token))
    self.current_author = author
  end

  def sign_out
    current_author.update_attribute(:session_token, Author.digest(Author.new_session_token))
    cookies.delete(:session_token)
    self.current_author = nil
  end

  def authenticate_author
    signin_controller = {controller: 'auth', action: 'signin'}
    redirect_to new_sessions_path unless author_signed_in?
  end

  def current_author=(author)
    @current_author = author
  end

  def current_author
    session_token = Author.digest(cookies[:session_token])
    @current_author ||= Author.find_by session_token: session_token
  end

  def author_signed_in?
    !self.current_author.nil?
  end
end
