class SessionsController < ApplicationController
  include SessionsHelper

  layout "sign_in"

  def new
  end

  def create
    author = Author.find_by email: author_params[:email]
    if author.authenticate author_params[:password]
      sign_in author
      redirect_to auth_path
    else
      flash[:danger] = "Failed to sign in"
      render :new
    end
  end

  def destroy
    sign_out
    redirect_to auth_path
  end

  private
    def author_params
      params.require(:author).permit(:email, :password)
    end
end
