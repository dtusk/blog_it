class CategoriesController < ApplicationController

  include SessionsHelper

  before_action :authenticate_author, except: [:show]
  before_action :set_category, only: [:show, :edit, :destroy, :update]
  layout "auth", except: :show

  def show
    @entries = @category.entries.where(published: true)
    if page_param = params[:page]
      @entries = @entries.page(page_param)
    else
      @entries = @entries.page(@entries.count / Kaminari.config.default_per_page)
    end
  end

  def index
    @categories = Category.order(:name)
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:success] = "You have successfully created a category"
    else
      flash[:danger] = "Some errors"
    end
    redirect_to :categories
  end

  def new
    @category = Category.new
  end

  def destroy
    if @category.destroy
      flash[:success] = "You have successfully destroyed a category"
    else
      flash[:danger] = "Some errors"
    end
    redirect_to :categories
  end

  def edit
  end

  def update
    @category.update(category_params)
    flash[:success] = "You have sucessfully updated a category"
    redirect_to categories_path
  end

  private

    def set_category
      begin
        @category = Category.friendly.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        render_404
      end
    end

    def category_params
      params.require(:category).permit(:name, :description)
    end
end
