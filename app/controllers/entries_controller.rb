class EntriesController < ApplicationController

  include SessionsHelper
  before_action :authenticate_author, expect: [:show, :facebook, :twitter]
  before_action :set_entry, only: [:show, :edit, :update, :destroy, :facebook, :twitter]
  layout "auth", except: :show
  after_action :save_clicks, only: [:facebook, :twitter]
  
  def show
    if @entry.published?
      @entry.clicks = @entry.clicks + 1
      @entry.save
    else
      render_404
    end
  end

  def destroy
    @entry.destroy
    flash[:success] = "Youu have successfully destroyed an entry!"
    redirect_to entries_path
  end

  def edit
  end

  def create
    @entry = Entry.new(entry_params)
    if @entry.save
      flash[:success] = "You have successfully created an entry!"
      redirect_to entries_path
    else
      flash.now[:danger] = "Some errors"
      render action: :new
    end
  end

  def update
    @entry.update(entry_params)
    redirect_to entries_path
  end

  def index
    @entries = Entry.all
  end

  def new
    @entry = Entry.new
  end

  def facebook
    @entry.facebook = @entry.facebook + 1
    redirect_to @entry
  end

  def twitter
    @entry.twitter = @entry.twitter + 1
    redirect_to @entry
  end

  private
    def set_entry
      begin
        @entry = Entry.friendly.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        render_404
      end
    end

    def save_clicks
      if @entry.published?
        @entry.save
      else
        render_404
      end
    end

    def entry_params
      params.require(:entry).permit(:title, :tag, :content, :published, :category_ids => [])      
    end
end
