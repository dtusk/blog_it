class SearchesController < ApplicationController
  def search
    if params[:query]
      @entries = Entry.contains(params[:query])
      if params[:page]
        @entries = @entries.page(params[:page])
      else
        @entries = @entries.page(1)
      end
    end
  end
end
