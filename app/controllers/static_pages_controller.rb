class StaticPagesController < ApplicationController
  
  def index
    @entries = Entry.where(published: true).order('created_at desc').joins(:categories)
    if page = params[:page]
      @entries = @entries.page(page)
    else
      @entries = @entries.page(@entries.count / Kaminari.config.default_per_page)
    end
  end

  def contact
  end

  def about
  end

end
