class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  before_filter :set_categories

  def render_404
    respond_to do |format|
      format.html { render 'errors/404', :layout => 'application', :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end
  
  private
    def set_categories
      @categories = Category.order(:name)
    end
end
