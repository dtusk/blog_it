class MediaController < ApplicationController

  layout "auth"

  include SessionsHelper
  before_action :authenticate_author, except: [:show]
  before_action :set_medium, only: [:show, :edit, :update, :destroy]

  def index
    @media = Medium.all
  end

  def show
  end

  def update
    @medium.update(medium_params)
    redirect_to media_path
  end

  def create
    @medium = Medium.new(medium_params)
    if @medium.save
      flash[:success] = "You have sucessfully created a media"
    end
    redirect_to media_path
  end

  def new
    @medium = Medium.new
  end

  def edit
  end

  def destroy
    @medium.destroy
    flash[:success] = "You have sucessfully destroyed a media"
    redirect_to media_path
  end

  private
    def set_medium
      @medium = Medium.find(params[:id])
    end

    def medium_params
      params.require(:medium).permit(:title, :description, :image)
    end
end
