require 'rails_helper'

RSpec.describe Entry, :type => :model do

  before { @entry = Entry.new(title: 'Foo Bar', content: 'Lorem ipsum dolorit', tag: 'foo, bar', clicks: 0, published: true) }

  subject { @entry }

  it { should respond_to(:title) }
  it { should respond_to(:slug) }
  it { should respond_to(:tag) }
  it { should respond_to(:content) }
  it { should respond_to(:clicks) }
  it { should respond_to(:published) }
  it { should respond_to(:author) }
  it { should respond_to(:categories) }
  it { should be_valid }

  context 'when title' do
    describe 'is not present' do
      before { @entry.title = '' }
      it { should_not be_valid }
    end

    describe 'is blank' do
      before { @entry.title = ' ' }
      it { should_not be_valid }
    end

  end

  context 'when tag' do
    describe 'is not present' do
      before { @entry.tag = '' }
      it { should_not be_valid }
    end
  end

  context 'when content' do
    describe 'is not present' do
      before { @entry.content = '' }
      it { should_not be_valid }
    end
  end

  context 'when published' do
    describe 'is not present' do
      before { @entry.published = nil }
      it { should_not be_valid }
    end
  end

  context 'when clicks' do
    describe 'is not present' do
      before { @entry.clicks = nil }
      it { should_not be_valid }
    end
    
    describe 'is below zero' do
      before { @entry.clicks = -1 }
      it { should_not be_valid }
    end
  end
end
