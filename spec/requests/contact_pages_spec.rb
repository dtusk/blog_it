require 'rails_helper'

RSpec.describe "Contact", :type => :request do
  describe "GET /contact" do
    it "should be valid" do
      visit contact_path
      expect(page).to have_title('Contact')
      expect(page).to have_selector('h2', text: 'Contact')
      expect(page).to have_selector('.active', text: 'contact')
    end
  end
end
