# Read about factories at https://github.com/thoughtbot/factory_girl

require 'faker'

FactoryGirl.define do

  prepared_for_slug = Faker::Lorem.words.to_s

  factory :entry do
    title prepared_for_slug
    slug Faker::Internet.slug(prepared_for_slug, '-')
    tag { Faker::Lorem.words }
    content { Faker::Lorem.sentences }
    clicks {  Faker::Number.number(3) }
    published [true,false].sample
    categories { [FactoryGirl.create(:category)] } 
    author
  end
end
