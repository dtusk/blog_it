# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :categories_entry, :class => 'CategoriesEntries' do
    category nil
    entry nil
  end
end
