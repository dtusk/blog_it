class AddTextileLatexToEntries < ActiveRecord::Migration
  def change
    add_column :entries, :textile, :text
    add_column :entries, :latex, :text
  end
end
