class CreateCategoriesEntries < ActiveRecord::Migration
  def change
    create_table :categories_entries do |t|
      t.references :category, index: true
      t.references :entry, index: true

      t.timestamps
    end
  end
end
