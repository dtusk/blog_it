class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.string :title
      t.string :slug
      t.string :tag
      t.text :content
      t.integer :clicks, null: false, default: 0
      t.boolean :published, null: false, default: false
      t.references :author, index: true

      t.timestamps
    end
    add_index :entries, :slug, unique: true
  end
end
