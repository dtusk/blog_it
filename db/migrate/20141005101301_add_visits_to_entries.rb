class AddVisitsToEntries < ActiveRecord::Migration
  def change
    add_column :entries, :facebook, :integer, null: false, default: 0
    add_column :entries, :twitter, :integer, null: false, default: 0
  end
end
