# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141006191503) do

  create_table "authors", force: true do |t|
    t.string   "email"
    t.string   "session_token"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
  end

  add_index "categories", ["name"], name: "index_categories_on_name", unique: true
  add_index "categories", ["slug"], name: "index_categories_on_slug", unique: true

  create_table "categories_entries", force: true do |t|
    t.integer  "category_id"
    t.integer  "entry_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "categories_entries", ["category_id"], name: "index_categories_entries_on_category_id"
  add_index "categories_entries", ["entry_id"], name: "index_categories_entries_on_entry_id"

  create_table "entries", force: true do |t|
    t.string   "title"
    t.string   "slug"
    t.string   "tag"
    t.text     "content"
    t.integer  "clicks",     default: 0,     null: false
    t.boolean  "published",  default: false, null: false
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "textile"
    t.text     "latex"
    t.integer  "facebook",   default: 0,     null: false
    t.integer  "twitter",    default: 0,     null: false
  end

  add_index "entries", ["author_id"], name: "index_entries_on_author_id"
  add_index "entries", ["slug"], name: "index_entries_on_slug", unique: true

  create_table "media", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
